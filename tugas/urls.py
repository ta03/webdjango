from django.urls import path
from tugas import views
from tugas.views import FileUploadView

urlpatterns = [
    path('', views.index, name='index'),
    # path('result/', views.result, name='result'),
    path('upload/', FileUploadView.as_view())
]