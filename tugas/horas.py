def split(word):
    return [char for char in word]

def generateString(NAMA, elektro):
    nama = NAMA.split(" ")
    elektro = split(elektro)
    i = 0
    a = 0
    while(i < len(elektro)):
        if elektro[i] == 'A' or elektro[i] == 'I' or elektro[i] == 'U' or elektro[i] == 'E' or elektro[i] == 'O':
            elektro[i] = nama[a]
            a += 1
            if a == len(nama):
                a = 0
        i += 1
    return elektro


    # Driver code
if __name__ == "__main__":
    NAMA = "AMSAL SUGIHAN SITUMORANG"
    elektro = "TEKNIK ELEKTRO"
    hasil = generateString(NAMA, elektro)
    print(''.join(hasil))
